import { createRouter, createWebHistory } from "vue-router";
import Home from "@/views/Home.vue";
import Login from "@/views/Login.vue";
import OutSide from "@/views/OutSide.vue";
import { useAuthStore } from "@/stores/auth";

const routes = [
  {
    path: "/",
    name: "Home",
    meta: {
      title: "PostList",
    },
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    meta: {
      title: "Login",
    },
    component: Login,
  },
  {
    path: "/outside",
    name: "OutSide",
    meta: {
      title: "Outside",
    },
    component: OutSide,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name !== "Login" && !isAuth()) next({ name: "Login" });
  else next();
});

router.afterEach((to) => {
  window.document.title = to.meta?.title;
  window.scrollTo(0, 0);
});

function isAuth() {
  const store = useAuthStore();
  return store.user.isLogin ? true : false;
}

export default router;
