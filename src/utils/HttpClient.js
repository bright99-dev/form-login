import axios from "axios";

const HttpClient = axios.create({
  baseURL: "https://js-post-api.herokuapp.com/api",
  timeout: 10000,
});

HttpClient.defaults.headers.post["Content-Type"] = "application/json";
HttpClient.defaults.headers.post["Accept"] = "application/json";

export default HttpClient;
