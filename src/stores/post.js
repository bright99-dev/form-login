import { defineStore } from "pinia";
import HttpClient from "@/utils/HttpClient";
import { ElNotification } from "element-plus";

export const usePostStore = defineStore("post", {
  state: () => ({
    postModel: {
      id: "",
      title: "",
      author: "",
      description: "",
      imageUrl: null,
      createdAt: "",
      updatedAt: "",
    },
    postList: [],
    pagination: {
      currentPage: null,
      pageSize: null,
      totalPost: null,
    },
  }),
  actions: {
    async fetchPostListByCurrentPage() {
      try {
        const res = await HttpClient.get(
          `/posts?_page=${this.pagination.currentPage}&_limit=${this.pagination.pageSize}`
        );
        this.pagination.totalPost = res.data.pagination._totalRows;
        this.postList = await this.processData(res.data.data);
      } catch (error) {
        this.openNotificationError();
        console.log(error);
      }
    },
    async fetchDetailsPost(id) {
      try {
        const res = await HttpClient.get(`/posts/${id}`);
        this.postModel = res.data;
      } catch (error) {
        this.openNotificationError();
        console.log(error);
      }
    },
    async createPost() {
      try {
        await HttpClient.post("/posts", this.postModel);
        this.fetchPostListByCurrentPage();
        this.openNotificationCreate();
        this.resetModel();
      } catch (error) {
        this.openNotificationError();
        console.log(error);
      }
    },
    async updatePost() {
      try {
        await HttpClient.patch(`/posts/${this.postModel.id}`, this.postModel);
        this.fetchPostListByCurrentPage();
        this.openNotificationUpdate();
      } catch (error) {
        this.openNotificationError();
        console.log(error);
      }
    },
    async deletePost() {
      try {
        await HttpClient.delete(`/posts/${this.postModel.id}`);
        this.openNotificationDelete();
        this.fetchPostListByCurrentPage();
      } catch (error) {
        this.openNotificationError();
        console.log(error);
      }
    },
    async filterTitleLike(postTitle, currentPage) {
      try {
        const res = await HttpClient.get(
          `/posts?title_like=${postTitle}&_page=${currentPage}&_limit=${this.pagination.pageSize}`
        );
        this.postList = await this.processData(res.data.data);
        this.pagination.totalPost = res.data.pagination._totalRows;
      } catch (error) {
        this.openNotificationError();
      }
    },
    async processData(potsList) {
      var newPostList = potsList.map((post) => {
        return {
          id: post?.id,
          title: post?.title,
          author: post?.author,
          description: post.description?.slice(0, 50),
          imageUrl: post.imageUrl,
        };
      });
      return newPostList;
    },
    resetModel() {
      this.postModel = {
        id: "",
        title: "",
        author: "",
        description: "",
        imageUrl: null,
        createdAt: "",
        updatedAt: "",
      };
    },
    openNotificationCreate() {
      ElNotification({
        title: "Success",
        message: "Created Successfully !!!",
        type: "success",
      });
    },
    openNotificationUpdate() {
      ElNotification({
        title: "Success",
        message: "Updated Successfully !!!",
        type: "success",
      });
    },
    openNotificationDelete() {
      ElNotification({
        title: "Success",
        message: "Deleted Successfully !!!",
        type: "success",
      });
    },
    openNotificationError() {
      ElNotification({
        title: "Error",
        message: "Something error occurred !!!",
        type: "error",
      });
    },
  },
});
