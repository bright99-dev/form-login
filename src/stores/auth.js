import { defineStore } from "pinia";
import router from "@/router";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    user: JSON.parse(localStorage.getItem("user")) ?? {
      username: "",
      password: "",
      isLogin: false,
    },
  }),
  actions: {
    login() {
      if (
        this.user.username === "ADMIN" &&
        this.user.password === "123456"
      ) {
        this.user.isLogin = true;
        localStorage.setItem("user", JSON.stringify(this.user));
        router.push("/");
      }else{
        alert('Please Enter Username & Password Valid')
      }
    },
    logout() {
      this.user.isLogin = false;
      localStorage.removeItem("user");
      router.push("/login");
    },
  },
});
